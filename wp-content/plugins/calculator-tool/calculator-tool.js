;(function($) {
	$(function() {
		var loanSlider = $("#loan_application_base_amount");
		if (!loanSlider.length) {
			return;
		}
		loanSlider.slider({
			id: 'amount_slider'
		}).on('slide', loan_changed);

		var periodSlider = $("#loan_application_period");
		periodSlider.slider({
			id: 'amount_period'
		}).on('slide', period_changed);

		function loan_changed() {
			$(this).prev().find('.slider-handle').not('.hide').html($(this).val() + ' €');
			calculate();
		}
		function period_changed() {
			$(this).prev().find('.slider-handle').not('.hide').html($(this).val() + ' días');
			calculate();
		}
		function calculate() {
			var amount = parseFloat($('#loan_application_base_amount').val());
			var period = parseInt($('#loan_application_period').val(), 10);
			var data = $('.calculator-tool').data();
			var loan_fee = 0;

			if(amount>100){
				loan_fee = round((((((data.max_sum - amount) / data.sum_step) + ((data.max_period - period) / data.period_step)) * data.percentage_step + data.price_percentage) / 100) * period * amount, 2);
			};

			$('.calculator-tool .loan_amount').text(amount.toString().replace('.', ',') + ' €');
			$('.calculator-tool .loan_price').text(loan_fee.toString().replace('.', ',') + ' €');
			var total = round(amount + loan_fee, 2);
			$('.calculator-tool .loan_net_amount').text(total.toString().replace('.', ',') + ' €');
			//Round towards the nearest neighbor, unless both neighbors are equidistant, in which case round away from zero (up).

		}

		function round(value, exp) {
			if (typeof exp === 'undefined' || +exp === 0)
				return Math.round(value);

			value = +value;
			exp = +exp;

			if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
				return NaN;

			// Shift
			value = value.toString().split('e');
			value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

			// Shift back
			value = value.toString().split('e');
			return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
		}

		loan_changed.apply(loanSlider);
		period_changed.apply(periodSlider);

		var loanEmail = $('#loan-email');
		var form = $(loanEmail).closest('form');
		validateForm(form);

		$(form).on('submit', function(e){
			
			$('input', $(this)).trigger('validate');

			if($('.error', $(this)).length){
				e.preventDefault();
			}
		});

		function validateForm(form) {
			var _this = this;

			var notEmptyTest = function(elem){
				return $(elem).val().length && $(elem).val()!='';
			}
			var regexTest = function(elem, regex){
				return notEmptyTest($(elem)) && regex.test($(elem).val());
			}

			var markError = function(elem, test, message){
				if(test) {
					$(elem).removeClass('error');
				}
				else {
					$(elem).addClass('error');
				}
			}

			$(form).each(function(i, form){
				$('input:not([type="hidden"])', form).on('validate', function(){
					markError(this, notEmptyTest($(this)) );
				});

				$('input[type="email"]', form).off('validate').on('validate', function(){
					var regex = /^[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+$/;
					markError(this, regexTest($(this), regex));
				});

				$('input:not([type="hidden"])', form).on('change blur', function(){
					$(this).trigger('validate');
				});
			});
		}
		

	});
})(jQuery);