<?php

the_posts_pagination(array(
	'prev_text' => __('Página anterior', 'holaluz'),
	'next_text' => __('Página siguiente', 'holaluz'),
	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __('Página', 'holaluz') . ' </span>',
));

?>
<a href="#header" id="btn-go-to-top"></a>
