<script>
	fbq('track', 'Purchase', {
		value: 247.35,
		currency: 'USD'
	});
	ga('send', 'pageview', '/application-sent.html');
</script>

<main role="main">
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div class="section-title"><?php the_title() ?></div>
		<div class="container registration">

			<p><strong>Gracias.</strong></p>
			<p>Tu solicitud ha sido enviada correctamente y tu préstamo ha sido aprobado. Te enviaremos la información a tu correo electrónico.</p>
			
		</div>
		<?php
	endwhile;
	endif;
	?>
</main>