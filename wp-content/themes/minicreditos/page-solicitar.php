<?php
/*
 * Template Name: Solicitud Crédito
 */

/* Variables de configuracion */
require_once(dirname(__FILE__) . '/libs/php-iban/php-iban.php');

//$api_url    = 'https://www.creditocajero.es/api/v1/loans';
$api_url    = 'https://staging.creditocajero.es/api/v1/loans';
$api_key    = '38f7e208ddae53e80442b3692fed58bb';
$secret_key = '94bbf02771120bc7864632901b7c6c73';
$transaction_id = '';
	
if(isset($_COOKIE['source'])) {
	$transaction_id = $_COOKIE['source'];
}

$template_form = 'page-solicitar-form';
$template_result_ok = 'page-solicitar-result-ok';
$template_result_ko = 'page-solicitar-result-ko';

/* Configuración desplegables */

$months = array();
$months[1]="Enero";
$months[2]="Febrero";
$months[3]="Marzo";
$months[4]="Abril";
$months[5]="Mayo";
$months[6]="Junio";
$months[7]="Julio";
$months[8]="Agosto";
$months[9]="Septiembre";
$months[10]="Octubre";
$months[11]="Noviembre";
$months[12]="Diciembre";

$gender = array();
$gender['male'] = 'Hombre';
$gender['female'] = 'Mujer';

$ownership_type = array();
$ownership_type["owner_with_mortage"]="Propietario con hipoteca";
$ownership_type["owner_without_mortage"]="Propietario sin hipoteca";
$ownership_type["rent"]="Alquiler";
$ownership_type["other"]="Otros";

$occupation_type = array();
$occupation_type["unemployed"]="Desempleado";
$occupation_type["temporary"]="Temporal";
$occupation_type["self_employed"]="Autónomo";
$occupation_type["pensioner"]="Pensionista";
$occupation_type["indefinite"]="Indefinido";
$occupation_type["no_work"]="No trabaja";

/* Codigo */

$timestamp = time();

$template = $template_form;

$default_data = array(
	'api_key' => $api_key,
	'timestamp' => $timestamp,
	'signature' => '',
	'locale' => 'es',
	'customer' => array(
		'email' => $_POST['email'],
		'password' => '',
		'first_name' => '',
		'surname' => '',
		'last_name' => '',
		'personal_code' => '',
		'birth_date' => array(
			'day'=>'',
			'month'=>'',
			'year'=>'',
		),
		'phone_number' => '',
		'bank_account_number' => '',
		'gender' => '',
		'zip_code' => '',
		'city' => '',
		'street' => '',
		'house_number' => '',
		'house_number_appendix' => '',
		'home_ownership_type' => '',
		'occupation_type' => '',
		'current_employment_since' => array(
			'day'=> '',
			'month'=>'',
			'year'=>'',
		),
		'net_income' => '',
		'dependant_count' => '',
		'remuneration_date' => '',
	),
	'loan' => array(
		'amount' => $_POST['amount'],
		'period' => $_POST['period'],
		'transaction_id' => $transaction_id,
	),
);

$validation_messages = array();

if ( !empty($_POST) && $_POST['step'] )
{
	$signature = $api_key . $timestamp . $secret_key;
	$signature = base64_encode(md5($signature,true));
	$post_data = array(
		'signature' => $signature,
		'customer' => array(
			'email' => trim($_POST['email']),
			'password' => trim($_POST['customer']['password']),
			'first_name' => trim($_POST['customer']['first_name']),
			'surname' => trim($_POST['customer']['surname']),
			'last_name' => trim($_POST['customer']['last_name']),
			'personal_code' => trim($_POST['customer']['personal_code']),
			'birth_date' => array(
				'day'=> (int)trim($_POST['customer']['birth_date']['day']),
				'month'=> (int)trim($_POST['customer']['birth_date']['month']),
				'year'=> (int)trim($_POST['customer']['birth_date']['year']),
			),
			'phone_number' => str_replace(' ', '', trim($_POST['customer']['phone_number'])),
			'bank_account_number' => trim($_POST['customer']['bank_customer_number_without_prefix']),
			'gender' => trim($_POST['customer']['gender']),
			'zip_code' => trim($_POST['customer']['zip_code']),
			'city' => trim($_POST['customer']['city']),
			'street' => trim($_POST['customer']['street']),
			'house_number' => trim($_POST['customer']['house_number']),
			'house_number_appendix' => trim($_POST['customer']['house_number_appendix']),
			'home_ownership_type' => trim($_POST['customer']['home_ownership_type']),
			'occupation_type' => trim($_POST['customer']['occupation_type']),
			'current_employment_since' => array(
				'day'=> 1,
				'month'=> (int)trim($_POST['customer']['current_employment_since']['month']),
				'year'=> (int)trim($_POST['customer']['current_employment_since']['year']),
			),
			'net_income' => trim($_POST['customer']['net_income']),
			'dependant_count' => trim($_POST['customer']['dependant_count']),
			'remuneration_date' => trim($_POST['customer']['remuneration_date']),
		),
		'loan' => array(
			'amount' => trim($_POST['amount']),
			'period' => trim($_POST['period']),
			'transaction_id' => $transaction_id,
		),
	);

	$data = array_merge( $default_data, $post_data );

	if ( empty( $data['customer']['first_name'] ) || strlen( $data['customer']['first_name'] ) < 2 || strlen( $data['customer']['first_name'] ) > 255 )
	{
		$validation_messages['customer_first_name'] = 'Es demasiado corto (2 caracteres mínimo)';
	}

	if ( empty( $data['customer']['last_name'] ) || strlen( $data['customer']['last_name'] ) < 2 || strlen( $data['customer']['first_name'] ) > 255)
	{
		$validation_messages['customer_last_name'] = 'Es demasiado corto (2 caracteres mínimo)';
	}

	if ( !empty( $data['customer']['surname'] ) && (strlen( $data['customer']['surname'] ) < 2 || strlen( $data['customer']['surname'] ) > 255) )
	{
		$validation_messages['customer_surname'] = 'Es demasiado corto (2 caracteres mínimo)';
	}

	if ( empty( $data['customer']['personal_code'] ) || !preg_match("/\A([xyXY]\-?|\d)\d{7}\-?[a-zA-Z]\z/i", $data['customer']['personal_code']) )
	{
		$validation_messages['customer_personal_code'] = 'No es válido';
	}

	if ( empty( $data['customer']['phone_number']) || !preg_match("/\A(6[0-9]|7[1-9])[0-9]{7}\z/i", $data['customer']['phone_number']) )
	{
		$validation_messages['customer_phone_number'] = 'No es válido';
	}

	$currentYear = date('Y');
	$currentMonth = date('n');
	$currentDay = date('j');

	$isOver18 = false;
	if( $currentYear - $data['customer']['birth_date']['year'] > 18 ) {
		$isOver18 = true;
	}else if( $currentYear - $data['customer']['birth_date']['year'] == 18 && $currentMonth - $data['customer']['birth_date']['month'] > 0 ){
		$isOver18 = true;
	}else if( $currentYear - $data['customer']['birth_date']['year'] == 18 && $currentMonth - $data['customer']['birth_date']['month'] == 0 && $currentDay - $data['customer']['birth_date']['day'] >= 0){
		$isOver18 = true;
	}

	$isBelow80 = false;
	if( $currentYear - $data['customer']['birth_date']['year'] < 80 ) {
		$isBelow80 = true;
	}else if( $currentYear - $data['customer']['birth_date']['year'] == 80 && $currentMonth - $data['customer']['birth_date']['month'] < 0 ){
		$isBelow80 = true;
	}else if( $currentYear - $data['customer']['birth_date']['year'] == 80 && $currentMonth - $data['customer']['birth_date']['month'] == 0 && $currentDay - $data['customer']['birth_date']['day'] <= 0){
		$isBelow80 = true;
	}

	if ( !checkdate( $data['customer']['birth_date']['month'], $data['customer']['birth_date']['day'], $data['customer']['birth_date']['year'] ) || !($isOver18 && $isBelow80)  )
	{
		$validation_messages['customer_birth_date'] = 'No es válido';
	}

	if ( empty( $data['customer']['street']) )
	{
		$validation_messages['customer_street'] = 'No puede estar en blanco';
	}

	if ( empty( $data['customer']['house_number'] ) || !preg_match("/\A[1-9][0-9]*\z/i", $data['customer']['house_number']) )
	{
		$validation_messages['customer_house_number'] = 'No puede estar en blanco';
	}

	if ( empty( $data['customer']['zip_code']) || !preg_match("/\A(0[1-9]|[1-4]\d|5[0-2])\d{3}\z/i", $data['customer']['zip_code']) )
	{
		$validation_messages['customer_zip_code'] = 'No es válido';
	}

	if ( empty( $data['customer']['city']) )
	{
		$validation_messages['customer_city'] = 'No puede estar en blanco';
	}

	if ( empty( $data['customer']['password']) || strlen( $data['customer']['password'] ) < 6 || strlen( $data['customer']['password'] ) > 20 )
	{
		$validation_messages['customer_password'] = 'Es demasiado corto (6 caracteres mínimo)';
	}

	$isInPast = false;
	if($data['customer']['current_employment_since']['year'] < $currentYear){
		$isInPast = true;
	}else if($data['customer']['current_employment_since']['year'] == $currentYear && $data['customer']['current_employment_since']['month'] <= $currentMonth){
		$isInPast = true;
	}

	if ( !checkdate( $data['customer']['current_employment_since']['month'], $data['customer']['current_employment_since']['day'], $data['customer']['current_employment_since']['year'] ) || !$isInPast )
	{
		$validation_messages['customer_current_employment_since'] = 'No es válido';
	}

	if ( !is_numeric( $data['customer']['net_income']) || $data['customer']['net_income'] < 100 || $data['customer']['net_income'] > 10000 )
	{
		$validation_messages['customer_net_income'] = 'No puede estar en blanco';
	}

	if ( !is_numeric( $data['customer']['dependant_count']) )
	{
		$validation_messages['customer_dependant_count'] = 'No es un número';
	}

	if ( !is_numeric( $data['customer']['remuneration_date']) || $data['customer']['remuneration_date'] < 1 || $data['customer']['remuneration_date'] > 31 )
	{
		$validation_messages['customer_remuneration_date'] = 'No es válido';
	}

	$iban = 'ES'.$data['customer']['bank_account_number'];
	$ibanParts = iban_get_parts($iban);

	$bank_code = $ibanParts['bank'];
	$branch_code = $ibanParts['branch'];
	$c1 = (int)substr($ibanParts['nationalchecksum'], 0, 1);
	$c2 = (int)substr($ibanParts['nationalchecksum'], 1, 1);
	$account = (int)substr($ibanParts['account'], 2);

	$result = internal_checksum($bank_code.$branch_code) == $c1 && internal_checksum($account) == $c2;

	if ( empty( $data['customer']['bank_account_number'] ) || !verify_iban($iban, $machine_format_only=false) || !$result)
	{
		$validation_messages['customer_bank_account_number'] = 'No es válido';
	}

	if ( empty($validation_messages) )
	{
		$data_to_post = $data;

		$data_to_post['customer']['birth_date'] =
			str_pad( trim($data['customer']['birth_date']['day']), 2, "0", STR_PAD_LEFT) . '.' .
			str_pad( trim($data['customer']['birth_date']['month']), 2, "0", STR_PAD_LEFT) . '.' .
			str_pad( trim($data['customer']['birth_date']['year']), 4, "0", STR_PAD_LEFT);

		$data_to_post['customer']['phone_number'] = '+34' . trim($_POST['customer']['phone_number']);

		$data_to_post['customer']['current_employment_since'] =
			str_pad( trim($data['customer']['current_employment_since']['day']), 2, "0", STR_PAD_LEFT) . '.' .
			str_pad( trim($data['customer']['current_employment_since']['month']), 2, "0", STR_PAD_LEFT) . '.' .
			str_pad( trim($data['customer']['current_employment_since']['year']), 4, "0", STR_PAD_LEFT);

		$data_to_post['customer']['bank_account_number'] = 'ES' . $data_to_post['customer']['bank_account_number'];

		$remote = wp_remote_post(
			$api_url,
			array(
				'body' => $data_to_post,
			)
		);

		if ( is_wp_error($remote) )
		{
			$template = $template_result_ko;
		}
		elseif ( $remote['response']['code'] != 200 )
		{
			$status_code = $remote['response']['code'];
			$response = json_decode($remote['body']);
			$template = $template_result_ko;
		}
		else
		{
			$template = $template_result_ok;
		}

		// $file = get_template_directory() . "/log.txt";
		// $time = date("F jS Y, H:i", time() + 60*60);
		// $open = @fopen($file, "a"); 
		// $write = @fputs($open, $time . PHP_EOL . 'personal code: ' . var_export($data['customer']['personal_code'], true) . PHP_EOL . PHP_EOL . var_export($remote, true) . PHP_EOL . PHP_EOL); 
		// @fclose($open);
	}
}
else
{
	$data = $default_data;
}

function validation_message( $field )
{
	global $validation_messages;

	if ( isset($validation_messages[ $field ] ) )
	{
		return '<span role="alert" class="wpcf7-not-valid-tip">'.$validation_messages[$field].'</span>';
	}
}

function internal_checksum( $code ) {
	$weights = [0, 0, 1, 2, 4, 8, 5, 10, 9, 7, 3, 6];
	$digits = rjust($code, count($weights), '0');
	$digits = str_split($digits);
	$checksum = 0;

	for($i=0; $i < count($weights); $i++){
		$checksum += (int)$digits[$i]*(int)$weights[$i];
	}
	$checksum = $checksum % 11;

	return $checksum <= 1 ? $checksum : (11 - $checksum);
}

// by techblog.willshouse.com
function rjust($string,$total_length,$fillchar=' ')
{
    if(strlen($string) >= $total_length)
    {
        return $string;
    }   

    $total_length = intval($total_length);

    if( ! $total_length )
    {
        return $string;
    }

    if(!strlen($fillchar))
    {
        return $string;
    }

    while(strlen($fillchar) < $total_length)
    {
        $fillchar = $fillchar.$fillchar;
    }

    return substr($fillchar.$string, ( -1 * $total_length ));
}

?>
<?php get_header(); ?>
<?php require_once("$template.php")?>
<?php get_footer(); ?>
