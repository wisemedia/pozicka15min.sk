<?php
/**
 * The single post page template.
 */

get_header(); the_post(); wpb_set_post_views(get_the_ID()); ?>
	
	<div class="container">
		<div class="row">
			<main role="main" class="col-xs-12 col-md-8">
				<div class="post" itemscope itemtype="http://schema.org/BlogPosting">
					<div class="post-content" itemprop="articleBody">
						<?php the_content(); ?>
					</div>
					<div class="post-author">
						<?php echo get_avatar( get_the_author_meta( 'ID' ), 70 ); ?>
						<div>
							<span class="author-name"><?php echo get_the_author(); ?></span>
							<span class="post-date"><?php echo get_the_date(); ?></span>
						</div>
					</div>
				</div>
			</main>
			<div class="hidden-xs hidden-sm col-md-4">
				<div class="box-posts">
					<h3>Lo más leído</h3>
					<ul>
						<?php 
							$popularpost = new WP_Query( array( 'posts_per_page' => 2, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
						?>
						<?php if($popularpost->have_posts()) : ?>
							<?php while ( $popularpost->have_posts() ) : $popularpost->the_post(); ?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'latest-post'); ?></a>
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<p><?php the_excerpt(); ?></p>
									<a class="read-more" href="<?php the_permalink(); ?>">Sigue leyendo</a>
								</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</ul>
				</div>
				<div class="box-posts">
					<h3>Últimos posts</h3>
					<ul>
						<?php 
						   // the query
						   $the_query = new WP_Query( array(
						      'posts_per_page' => 2,
						      'post__not_in' => array($post->ID),
						   )); 
						?>
						<?php if ( $the_query->have_posts() ) : ?>
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<li>
									<a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'latest-post'); ?></a>
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
									<p><?php the_excerpt(); ?></p>
									<a class="read-more" href="<?php the_permalink(); ?>">Sigue leyendo</a>
								</li>
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	

<?php get_footer(); ?>