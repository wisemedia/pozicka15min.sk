<script>
	fbq('track', 'Lead', {
		value: 10.00,
		currency: 'USD'
	});
</script>

<main role="main">
	<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div class="section-title"><?php the_title() ?></div>
		<div class="container registration">

			<form method="post" novalidate>
				<input type="hidden" name="amount" value="<?=$data['loan']['amount']?>">
				<input type="hidden" name="period" value="<?=$data['loan']['period']?>">
				<input type="hidden" name="email" value="<?=$data['customer']['email']?>">
				<input type="hidden" name="step" value="1">

				<div class="registration-content">
					<fieldset>
						<legend class="text-center padding-left-right font-bold">Datos personales</legend>
						<div class="form-group">
							<label class="control-label" for="customer_first_name">Nombre</label>
							<div class="input-group">
								<span class="customer_first_name">
									<input type="text" name="customer[first_name]" value="<?=$data['customer']['first_name']?>" size="40" class="form-control" id="customer_first_name">
									<?php echo validation_message('customer_first_name') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce tu nombre tal y como aparece en tu DNI/NIE" data-original-title="Nombre"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_last_name">Primer apellido</label>
							<div class="input-group">
								<span class="customer_last_name">
									<input type="text" name="customer[last_name]" value="<?=$data['customer']['last_name']?>" size="40" class="form-control" id="customer_last_name">
									<?php echo validation_message('customer_last_name') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce tu primer apellido tal y como aparece en tu DNI/NIE" data-original-title="Primer apellido"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_surname">Segundo apellido</label>
							<div class="input-group">
								<span class="customer_surname">
									<input type="text" name="customer[surname]" value="<?=$data['customer']['surname']?>" size="40" class="form-control" id="customer_surname">
									<?php echo validation_message('customer_surname') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce tu segundo apellido tal y como aparece en tu DNI/NIE" data-original-title="Segundo apellido"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_gender">Género</label>
							<div class="input-group">
								<span class="customer_gender">
									<select name="customer[gender]" class="form-control" id="customer_gender">
										<?php foreach($gender as $key=>$value):?>
											<option value="<?=$key?>"<?if($data['customer']['gender']==$key):?> selected="selected"<?endif;?>><?=$value?></option>
										<?php endforeach;?>
									</select>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Selecciona tu género." data-original-title="Género"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_personal_code">DNI/NIE</label>
							<div class="input-group">
								<span class="customer_personal_code">
									<input type="text" name="customer[personal_code]" value="<?=$data['customer']['personal_code']?>" size="40" class="form-control" id="customer_personal_code">
									<?php echo validation_message('customer_personal_code') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el número tal y como aparece en tu DNI/NIE." data-original-title="DNI/NIE"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_phone_number_without_area_code">Teléfono móvil</label>
							<div class="input-group">
								<div class="input-group-addon">
									+34
								</div>
								<span class="customer_phone_number">
									<input type="text" name="customer[phone_number]" value="<?=$data['customer']['phone_number']?>" size="40" class="form-control" id="customer_phone_number_without_area_code">
									<?php echo validation_message('customer_phone_number') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el número sin espacios. Te enviaremos un código de confirmación para verificar el proceso." data-original-title="Teléfono móvil"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_birth_date">Fecha de nacimiento</label>
							<div class="input-group">
								<span class="customer_birth_date_day">
									<select name="customer[birth_date][day]" class="form-control date-select day">
										<option value="">Día</option>
										<?php for($x=1;$x<=31;$x++):?>
											<option value="<?=$x?>"<?if($data['customer']['birth_date']['day']==$x):?> selected="selected"<?endif;?>><?=$x?></option>
										<?php endfor; ?>
									</select>
								</span>

								<span class="customer_birth_date_month">
									<select name="customer[birth_date][month]" class="form-control date-select month">
										<option value="">Mes</option>
										<?php foreach($months as $key=>$value):?>
											<option value="<?=$key?>"<?if($data['customer']['birth_date']['month']==$key):?> selected="selected"<?endif;?>><?=$value?></option>
										<?php endforeach;?>
									</select>
								</span>

								<span class="customer_birth_date_year">
									<select name="customer[birth_date][year]" class="form-control date-select year">
										<option value="">Año</option>
										<?php for($x=(intval(date('Y'))-18);$x>=1936;$x--):?>
											<option value="<?=$x?>"<?if($data['customer']['birth_date']['year']==$x):?> selected="selected"<?endif;?>><?=$x?></option>
										<?php endfor; ?>
									</select>
								</span>
								<?php echo validation_message('customer_birth_date') ?>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Selecciona tu fecha de nacimiento. Tienes que ser mayor de edad para solicitar un préstamo." data-original-title="Fecha de nacimiento"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_street">Dirección</label>
							<div class="input-group">
								<span class="customer_street">
									<input type="text" name="customer[street]" value="<?=$data['customer']['street']?>" size="40" class="form-control" id="customer_street">
									<?php echo validation_message('customer_street') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el nombre de tu calle." data-original-title="Dirección"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_house_number">Número</label>
							<div class="input-group">
								<span class="customer_house_number">
									<input type="text" name="customer[house_number]" value="<?=$data['customer']['house_number']?>" size="40" class="form-control" id="customer_house_number">
									<?php echo validation_message('customer_house_number') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce la información adicional de tu dirección." data-original-title="Número"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_house_number_appendix">Piso y puerta</label>
							<div class="input-group">
								<span class="customer_house_number_appendix">
									<input type="text" name="customer[house_number_appendix]" value="<?=$data['customer']['house_number_appendix']?>" size="40" class="form-control" id="customer_house_number_appendix">
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce la información adicional para registrar correctamente tu dirección" data-original-title="Piso y puerta"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_zip_code">Código postal</label>
							<div class="input-group">
								<span class="customer_zip_code">
									<input type="text" name="customer[zip_code]" value="<?=$data['customer']['zip_code']?>" size="40" class="form-control" id="customer_zip_code">
									<?php echo validation_message('customer_zip_code') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el código postal de tu población" data-original-title="Código postal"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_city">Población</label>
							<div class="input-group">
								<span class="customer_city">
									<input type="text" name="customer[city]" value="<?=$data['customer']['city']?>" size="40" class="form-control" id="customer_city">
									<?php echo validation_message('customer_city') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el nombre de tu población." data-original-title="Población"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_home_ownership_type">Tipo de vivienda</label>
							<div class="input-group">
								<span class="customer_home_ownership_type">
									<select name="customer[home_ownership_type]" class="form-control" id="customer_home_ownership_type">
										<?php foreach($ownership_type as $key=>$value):?>
											<option value="<?=$key?>"<?if($data['customer']['home_ownership_type']==$key):?> selected="selected"<?endif;?>><?=$value?></option>
										<?php endforeach;?>
									</select>
								</span>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Selecciona el estado de tu vivienda actual" data-original-title="Tipo de vivienda"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_password">Contraseña</label>
							<div class="input-group">
								<span class="customer_password">
									<input type="password" name="customer[password]" value="<?=$data['customer']['password']?>" size="40" class="form-control" id="customer_password">
									<?php echo validation_message('customer_password') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce tu contraseña (mín. 6 caracteres). La utilizarás para iniciar sesión en tu área personal." data-original-title="Contraseña"></div>
							</div>
						</div>
					</fieldset>
				</div>

				<div class="registration-content">
					<fieldset>
						<legend class="text-center padding-left-right font-bold">Datos laborales y bancarios</legend>

						<div class="form-group">
							<label class="control-label" for="customer_occupation_type">Situación laboral</label>
							<div class="input-group">
								<span class="customer_occupation_type">
									<select name="customer[occupation_type]" class="form-control" id="customer_occupation_type">
										<?php foreach($occupation_type as $key=>$value):?>
											<option value="<?=$key?>"<?if($data['customer']['occupation_type']==$key):?> selected="selected"<?endif;?>><?=$value?></option>
										<?php endforeach;?>
									</select>
								</span>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Selecciona tu situación laboral actual" data-original-title="Situación laboral"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_current_employment_since">Fecha de inicio</label>
							<div class="input-group">
								<span class="customer_current_employment_since_month">
									<select name="customer[current_employment_since][month]" class="form-control date-select month">
										<option value="">Mes</option>
										<?php foreach($months as $key=>$value):?>
											<option value="<?=$key?>"<?if($data['customer']['current_employment_since']['month']==$key):?> selected="selected"<?endif;?>><?=$value?></option>
										<?php endforeach;?>
									</select>
								</span>

								<span class="customer_current_employment_since_year">
									<select name="customer[current_employment_since][year]" class="form-control date-select year">
										<option value="">Año</option>
										<?php for($x=intval(date('Y'));$x>=1936;$x--):?>
											<option value="<?=$x?>"<?if($data['customer']['current_employment_since']['year']==$x):?> selected="selected"<?endif;?>><?=$x?></option>
										<?php endfor; ?>
									</select>
									<?php echo validation_message('customer_current_employment_since') ?>
								</span>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Selecciona la fecha de inicio de tu situación laboral actual" data-original-title="Fecha de inicio"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_net_income">Ingresos mensuales netos</label>
							<div class="input-group">
								<span class="customer_net_income">
									<input type="number" name="customer[net_income]" value="<?=$data['customer']['net_income']?>" size="40" class="form-control" id="customer_net_income">
									<?php echo validation_message('customer_net_income') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce el importe neto declarado del titular." data-original-title="Ingresos mensuales netos"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_dependant_count">Personas a tu cargo</label>
							<div class="input-group">
								<span class="customer_dependant_count">
									<input type="number" name="customer[dependant_count]" value="<?=$data['customer']['dependant_count']?>" class="form-control" id="customer_dependant_count" min="0" max="10">
									<?php echo validation_message('customer_dependant_count') ?>
								</span>
								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Indica cuántas personas dependen de ti" data-original-title="Personas a tu cargo"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_remuneration_date">Día previsto de tu próximo ingreso</label>
							<div class="input-group">
								<span class="customer_remuneration_date">
									<select name="customer[remuneration_date]" class="form-control date-select">
										<?php for($x=1;$x<=31;$x++):?>
											<option value="<?=$x?>" <?if($x==$data['customer']['remuneration_date']):?> selected="selected"<?endif;?>><?=$x?></option>
										<?php endfor; ?>
									</select>
									<?php echo validation_message('customer_remuneration_date') ?>
								</span>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Indica el día del mes en que tienes previsto recibir tu próximo ingreso o en el que acostumbras a recibir tu ingreso habitual" data-original-title="Día previsto de tu próximo ingreso"></div>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label" for="customer_bank_customer_number_without_prefix">Número de cuenta</label>
							<div class="input-group">
								<div class="input-group-addon">
									ES
								</div>
								<span class="customer_bank_customer_number_without_prefix">
									<input type="text" name="customer[bank_customer_number_without_prefix]" value="<?=$data['customer']['bank_account_number']?>" size="40" class="form-control" id="customer_bank_customer_number_without_prefix">
									<?php echo validation_message('customer_bank_account_number') ?>
								</span>

								<div class="input-group-addon help" data-toggle="popover" data-placement="top" title="" data-content="Introduce tu cuenta con el código IBAN." data-original-title="Número de cuenta"></div>
							</div>
						</div>

						<div class="action-wrapper">
							<div class="action">
								<input type="submit" value="Siguiente" class="btn btn-default">
							</div>
						</div>
					</fieldset>

					<p class="text-center">
						<a target="_blank" href="<?=get_permalink(40)?>">Pulsando “Siguiente”, aceptas la Política de Privacidad</a>
					</p>

				</div>
			</form>

		</div>

		<?php
	endwhile;
	endif;
	?>
</main>