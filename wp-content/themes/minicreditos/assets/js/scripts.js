function popover() {
	jQuery('[data-toggle="popover"]').popover({
		trigger: "hover",
		container: "body"
	}).click(function(e) {
		jQuery(this).popover("toggle")
	});
}

jQuery(function(){
	popover();

	jQuery('.btn-menu').on('click', function(e){
		e.preventDefault();
		jQuery(this).toggleClass('active');
	});
})