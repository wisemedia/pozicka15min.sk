<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-T4XCW3Q');</script>
	<!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php wp_head(); ?>

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '262618227501387'); // Insert your pixel ID here.
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=262618227501387&ev=PageView&noscript=1"
	/></noscript>
	<!-- DO NOT MODIFY -->
	<!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/Blog">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T4XCW3Q"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<div class="page-wrapper">
		<?php
			$bg = '';
			if(is_single() && get_post_type()=='post') { 
				$bg = 'style="background-image:url('.get_the_post_thumbnail_url().');"'; 
			}elseif( (is_home() && get_option('page_for_posts')) || is_category() ){
				$img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'full'); 
    			$featured_image = $img[0];
				$bg = 'style="background-image:url('.$featured_image.');"'; 
			};
		?>

		<header id="header" itemscope itemtype="https://schema.org/WPHeader" <?php echo $bg; ?>>
			<div class="logo">
				<div class="container">
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" alt="minicréditos enlínea"/>
					</a>
					<a href="#" class="btn-menu"></a>
					<nav>
						<?php
						wp_nav_menu(array(
							'menu' => 'header',
							'menu_class' => 'clearfix',
							'depth' => 1,
							'theme_location' => 'header'
						));
						?>
					</nav>
				</div>
			</div>
			<div class="container">
				<div class="inner-header">
					<?php if (is_single()): ?>
						<h1><?php the_title(); ?></h1>
					<?php elseif (is_home()): ?>
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<h1><?php single_post_title(); ?></h1>
								<p class="hidden-xs hidden-sm"><?php echo get_post_field( 'post_content', get_option('page_for_posts') );?></p>
							</div>
						</div>
					<?php elseif(is_category()): ?>
						<div class="row">
							<div class="col-md-10 col-md-offset-1">
								<h1><?php single_cat_title(); ?></h1>
								<p class="hidden-xs hidden-sm"><?php $category = get_the_category(); echo $category[0]->description; echo category_description($post->ID); ?></p>
							</div>
						</div>
					<?php elseif (is_front_page()): ?>
					<h1 class="motto-1">¿Estás buscando minicréditos?</h1>
					<hr class="motto-hr"/>
					<h2 class="motto-2">¡Consigue aquí un mini crédito al instante!</h2>
						<?php //do_shortcode('[calculator_tool]'); ?>
					<?php endif; ?>
				</div>
			</div>
		</header>